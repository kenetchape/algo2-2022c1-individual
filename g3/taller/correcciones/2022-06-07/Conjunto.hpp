
template <class T>
Conjunto<T>::Conjunto():_cardinal(0), _raiz(nullptr) {}

template<class T>
void Conjunto<T>::deletear(Nodo* index){
    if (index != nullptr) {
        deletear(index->der);
        deletear(index->izq);
        delete index;
    }
}

template <class T>
Conjunto<T>::~Conjunto() {
    deletear(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    //assert(false);
    bool res= false;
    Nodo* index = _raiz;

    while ( _cardinal != 0 && (index->izq != nullptr || index->der != nullptr) && !res ){
        if ( index->valor < clave && index->der != nullptr ){
            index = index->der;
        }else if ( clave < index->valor && index->izq != nullptr ) {
            index = index->izq;
        }else {
            res = true;
        }
    }
    if (_cardinal == 0) {
        return res;
    } else {
        return index->valor == clave;
    }

}

// Pre:{ true }
template <class T>
void Conjunto<T>::insertar(const T& clave) {
    //assert(false);
    if ( _cardinal == 0 ) {
        Nodo* agregar = new Nodo(clave);
        _raiz = agregar;
        _cardinal++;
    } else {
        Nodo* index = _raiz;
        bool parar = false ;
        while ( (index->izq != nullptr || index->der != nullptr) && !parar) {
            if ( index->valor < clave && index->der != nullptr ){
                index = index->der;
            }else if ( clave < index->valor && index->izq != nullptr ) {
                index = index->izq;
            }else {
                parar = true;
            }
        }
        if ( index->valor < clave ) {
            Nodo* agregar = new Nodo(clave);
            index->der = agregar;
            _cardinal++;
        }
        if ( clave < index->valor ) {
            Nodo* agregar = new Nodo(clave);
            index->izq = agregar;
            _cardinal++;
        }
    }
}

// Pre:{ true }
template <class T>
void Conjunto<T>::remover(const T& clave) {
    //assert(false);
    Nodo* nodo_buscar = _raiz;
    Nodo* padre = nodo_buscar;
    string orientacion = "es raiz";
    if ( nodo_buscar->valor < clave && nodo_buscar->der != nullptr) {
        nodo_buscar = nodo_buscar->der;
        orientacion = "derecha";
    }
    if ( clave < nodo_buscar->valor && nodo_buscar->izq != nullptr) {
        nodo_buscar = nodo_buscar->izq;
        orientacion = "izquierda";
    }
    bool para = false;
    // Buscamos el nodo que apunta a clave:
    while ( (nodo_buscar->der != nullptr || nodo_buscar->izq != nullptr) && !para){
        if( nodo_buscar->valor < clave && nodo_buscar->der != nullptr ){
            padre = nodo_buscar;
            nodo_buscar = nodo_buscar->der;
            orientacion = "derecha";
        }else if( clave < nodo_buscar->valor && nodo_buscar->izq != nullptr ) {
            padre = nodo_buscar;
            nodo_buscar = nodo_buscar->izq;
            orientacion = "izquierda";
        }else {
            para = true;
        }
    }
    if ( nodo_buscar->valor == clave) {
        bool paso = false;
        // CASO: en el que es una hoja:
        if (nodo_buscar->izq == nullptr && nodo_buscar->der == nullptr ) {
            if (orientacion == "derecha") padre->der = nullptr;
            if (orientacion == "izquierda") padre->izq = nullptr;
            if (orientacion == "es raiz") _raiz = nullptr;
            delete nodo_buscar;
            paso = true;
        }
        // CASO: tiene AMBOS hijos:
        if ( !paso && nodo_buscar->izq != nullptr && nodo_buscar->der != nullptr ) {
            // Encontramos al antecesor inmediato, existe puesto que tiene ambos hijos:
            Nodo* suce = nodo_buscar->izq;
            padre = nodo_buscar;
            while (suce->der != nullptr) {
                padre = suce;
                suce = suce->der;
            }
            nodo_buscar->valor = suce->valor;

            // la variable padre, representa al padre del sucesor, en este contexto:
            if ( padre == nodo_buscar && suce->izq == nullptr ) {
                padre->izq = nullptr;
                // padre no es el nodo a buscar y el suce es una hoja:
            }else if (padre != nodo_buscar && suce->izq == nullptr ){
                padre->der = nullptr;
                // padre no es el nodo a buscar y el suce no es una hoja:
            }else if(padre != nodo_buscar && suce->izq != nullptr ){
                padre->der = suce->izq;
                // padre es el nodo a buscar y el suce no es hoja y por tanto tiene 1 hijo:
            }else {
                padre->izq = suce->izq;
            }
            delete suce;
            paso = true;
        }
        // CASO: tiene SOLO un hijo:
        if ( !paso && ((nodo_buscar->der != nullptr && nodo_buscar->izq == nullptr) || (nodo_buscar->der == nullptr && nodo_buscar->izq != nullptr) )){
            if (nodo_buscar->der != nullptr) {
                if (orientacion == "derecha") {
                    padre->der = nodo_buscar->der;
                    delete nodo_buscar;
                } else if (orientacion == "izquierda") {
                    padre->izq = nodo_buscar->der;
                    delete nodo_buscar;
                } else {
                    // caso en el que es la raiz:
                    _raiz = nodo_buscar->der;
                    delete nodo_buscar;
                }
            } else {
                if ( orientacion == "derecha" ){
                    padre->der = nodo_buscar->izq;
                    delete nodo_buscar;
                } else if (orientacion == "izquierda"){
                    padre->izq = nodo_buscar->izq;
                    delete nodo_buscar;
                } else {
                    _raiz = nodo_buscar->izq;
                    delete nodo_buscar;
                }
            }
        }
        _cardinal--;
    }
}

// Pre:{ clave\in Conjunto && tiene siguiente }
template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    //assert(false);
    Nodo* index = _raiz;
    Nodo* padre = index;
    if (index->valor < clave){
        index = index->der;
    }else if ( clave < index->valor ){
        index = index->izq;
    }

    // Se supone que la clave esta en el conjunto por lo que no hace falta ver si es NULL
    // Por lo tanto buscamos el nodo al que apunta dicha clave:
    while ( index->valor != clave ){
        if ( index->valor < clave ) {
            padre = index;
            index = index->der;
        }
        if ( clave < index->valor ) {
            padre = index;
            index = index->izq;
        }
    }

    if (index->der != nullptr) {
        index = index->der;

        while (index->izq != nullptr) {
            index = index->izq;
        }
        return index->valor;
    } else{
        return padre->valor;
    }

}

// Pre:{ Conjunto no vacio }
template <class T>
const T& Conjunto<T>::minimo() const {
    //assert(false);
    Nodo* index = _raiz;
    while ( index->izq != nullptr ) index = index->izq;
    return index->valor;
}

// Pre:{ Conjunto no vacio }
template <class T>
const T& Conjunto<T>::maximo() const {
    //assert(false);
    Nodo* index = _raiz;
    while ( index->der != nullptr ) index = index->der;
    return index->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    //assert(false);
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream& os) const {
    //assert(false);
    /*{
    if (_cardinal != 0) {
        if (_cardinal == 1) {
            os << _raiz->valor;
        }
        T minimo = this->minimo();
        T s = siguiente(minimo);
        T sig = s;
        os << minimo << " ";
        while (sig != this->maximo()) {
            sig = siguiente(sig);
            os << minimo << " ";
        }
    }
    }*/
}

// Parte de struct Nodo:

template<class T>
Conjunto<T>::Nodo::Nodo(const T &v): valor(v), izq(nullptr), der(nullptr) {}


