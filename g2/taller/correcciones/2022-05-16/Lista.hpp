#include "Lista.h"

Lista::Lista():_length(0), _primero(nullptr), _ultimo(nullptr) {
    _inicio = new Nodo();
    _final = new Nodo();
    _inicio->next = _final;
    _final->back = _inicio;
}

Lista::Lista(const Lista& l) : Lista() {*this = l;}

Lista& Lista::operator=(const Lista& aCopiar) {
    int largo = _length;
    for ( int i =0; i < largo ; i++ ) {
        eliminar(0);
    }

    _length = 0;

    for (int i = 0; i < aCopiar.longitud(); ++i) {
        int x = aCopiar.iesimo(i);
        this->agregarAtras(x);
    }
    return *this;
}

Lista::~Lista() {
    if (_length == 0 ){
        _inicio->next = nullptr;
        _final->back = nullptr;
        delete _inicio;
        delete _final;
    }else {

    Nodo* index = _inicio->next;
    _final->back = nullptr;
    _inicio->next = nullptr;

    while ( index->next != _final ) {
        Nodo* elim = index;
        index = index->next;

        elim->next->back = nullptr;
        elim->next = nullptr;
        elim->back = nullptr;
        delete elim;
    }

    index->back = nullptr;
    index->next = nullptr;
    delete index;

    delete _inicio;
    delete _final;
    }
}

void Lista::agregarAdelante(const int& elem) {
    _length++;
    Nodo* agregado = new Nodo(elem);
    agregado->next = _inicio->next;
    agregado->back = _inicio;
    //Aca termina la parte de poner el valor Nodo agregado
    _inicio->next->back = agregado;
    _inicio->next = agregado;
    _primero = &_inicio->next->valor;
    _ultimo = &_final->back->valor;

}

void Lista::agregarAtras(const int& elem) {
    _length++;
    Nodo* agregado = new Nodo(elem);
    agregado->next = _final;
    agregado->back = _final->back;
    // Analogo a lo de arriba
    _final->back->next = agregado;
    _final->back = agregado;

    _primero = &_inicio->next->valor;
    _ultimo = &_final->back->valor;
}
//Pre: 0 <= i < length(Lista)
void Lista::eliminar(Nat i) {
    _length--;
    Nodo* apunta_izquierda = _inicio;
    Nodo* apunta_derecha = _inicio->next->next;
    Nodo* apunta_eliminar = _inicio->next;
    for (Nat j = 0; j < i; j++ ){
        apunta_eliminar = apunta_eliminar->next;
        apunta_izquierda = apunta_izquierda->next;
        apunta_derecha = apunta_derecha->next;
    }
    //Aca se hace la parte de reenlazar los next's y back's
    apunta_derecha->back = apunta_izquierda;
    apunta_izquierda->next = apunta_derecha;

    apunta_eliminar->next = nullptr;
    apunta_eliminar->back = nullptr;
    delete apunta_eliminar;
}

int Lista::longitud() const {
    return _length;
}

const int& Lista::iesimo(Nat i) const {
    if (i < _length){
        Nodo* puntero_i = _inicio->next;
        for (Nat j = 0; j < i; ++j) puntero_i = puntero_i->next;
        return puntero_i->valor;
    }
}

int& Lista::iesimo(Nat i) {
    if (i < _length){
        Nodo* puntero_i = _inicio->next;
        for (Nat j = 0; j < i; ++j) puntero_i = puntero_i->next;
        return puntero_i->valor;
    }
}

void Lista::mostrar(ostream& o) {
    o << "[" ;
    Nodo* recorrer = _inicio->next;
    for (int i = 0; i < this->_length; ++i) {
        if (i == this->_length -1 ){
            o << recorrer->valor;
        }else{
            o << recorrer->valor << ", ";
        }
        recorrer = recorrer->next;
    }
    o << "]"<< endl;
}

Lista::Nodo::Nodo(const int& elem):next(nullptr),back(nullptr),valor(elem) {}

Lista::Nodo::Nodo():next(nullptr),back(nullptr) {}
