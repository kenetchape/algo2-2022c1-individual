#include <utility>
using uint = unsigned int;
using namespace std;
// Ejercicio 15
using Pos = pair<int, int>;
using pocion = pair<uint, uint>;
char ARRIBA = '^';
char ABAJO = 'v'; 
char DERECHA = '<';
char IZQUIERDA = '>';

// Debido a que es cuadrado, tenemos que casilleros es el natural que determina la dimension de la matriz.
class Juego {
public:
    Juego(uint casilleros, Pos pos_inicial);
    uint turno_actual();
    Pos posicion_jugador();
    void mover_jugador(char dir);
    void ingerir_pocion(uint movimientos,uint turnos);

private:
    Pos posicion_j_;
    uint dimension_;
    uint turno_actual_;
    pocion pocion_; // la primer posicion es movimientos, la segunda es la cantidad de turnos que aguanta la pocion
    uint movimientos_pocion;
};
// Supongo como precondicion que la posicion inicial estara en rango
Juego::Juego(uint casilleros, Pos pos_inicial):posicion_j_(pos_inicial),dimension_(casilleros),turno_actual_(0),pocion_(make_pair(0,0)),movimientos_pocion(0) {}
uint Juego::turno_actual() {
    return turno_actual_;
}
Pos  Juego::posicion_jugador() {
    return posicion_j_;
}
void Juego::mover_jugador(char dir) {
    if (pocion_.second == 0) {
        if (dir == ARRIBA & posicion_j_.first == 0) {}
        else {
            posicion_j_.first = pocion_.first + 1;
        }
        if (dir == ABAJO & posicion_j_.first == dimension_ - 1) {}
        else {
            posicion_j_.first = posicion_j_.first -1;
        }
        if (dir == IZQUIERDA & posicion_j_.second == 0) {}
        else {
            posicion_j_.second = posicion_j_.second - 1;
        }
        if (dir == DERECHA & posicion_j_.second == dimension_ - 1) {}
        else {
            posicion_j_.second = posicion_j_.second + 1;
        }
        turno_actual_++;
        pocion_.first = 0;
        movimientos_pocion = 0;
    }else if (movimientos_pocion > 0) {
        if (dir == ARRIBA & posicion_j_.first == 0) {}
        else {
            posicion_j_.first = pocion_.first + 1;
        }
        if (dir == ABAJO & posicion_j_.first == dimension_ - 1) {}
        else {
            posicion_j_.first = posicion_j_.first -1;
        }
        if (dir == IZQUIERDA & posicion_j_.second == 0) {}
        else {
            posicion_j_.second = posicion_j_.second - 1;
        }
        if (dir == DERECHA & posicion_j_.second == dimension_ - 1) {}
        else {
            posicion_j_.second = posicion_j_.second + 1 ;
        }
        movimientos_pocion--;
        } else {
        pocion_.second--;
        movimientos_pocion = pocion_.first - 1;
        if (dir == ARRIBA & posicion_j_.first == 0) {}
        else {
            posicion_j_.first = pocion_.first + 1;
        }
        if (dir == ABAJO & posicion_j_.first == dimension_ - 1) {}
        else {
            posicion_j_.first = posicion_j_.first -1;
        }
        if (dir == IZQUIERDA & posicion_j_.second == 0) {}
        else {
            posicion_j_.second = posicion_j_.second - 1;
        }
        if (dir == DERECHA & posicion_j_.second == dimension_ - 1) {}
        else {
            posicion_j_.second = posicion_j_.second + 1 ;
        }
    }
}
void Juego::ingerir_pocion(uint movimientos, uint turnos) {
    if (pocion_.second == 0 ) pocion_.second = turnos;
    pocion_.first = pocion_.first + movimientos;
    movimientos_pocion = pocion_.first;
}



