#include <iostream>
#include "list"
using namespace std;
using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10
// Clase Fecha
class Fecha {
  public:
    Fecha(uint mes, uint dia);
    uint mes();
    uint dia();
    bool operator==(Fecha o);
    void incrementar_dia();
  private:
    uint mes_;
    uint dia_;
};
Fecha::Fecha(uint mes, uint dia): mes_(mes),dia_(dia) {}
uint Fecha::mes() {
    return mes_;
}
uint Fecha::dia() {
    return dia_;
}
ostream& operator<<(ostream& os,Fecha f ){
    os << f.dia() << "/"<<f.mes();
    return os;
}
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia & igual_mes;
}
void Fecha::incrementar_dia() {
    if(dias_en_mes(this->mes_) == this->dia_ ){
        if (mes_ + 1 % 12 == 0) {
            mes_ = 12;
        }else {
            mes_++;
        }
        dia_ = 1;
    }else{
        dia_++;
    }
}

// Ejercicio 11, 12
// Clase Horario
class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);
private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min): hora_(hora) , min_(min) {};
uint Horario::min() {
    return this->min_;
}
uint Horario::hora(){
    return this->hora_;
}
bool Horario::operator==(Horario h) {
    return hora_ == h.hora() & min_ == h.min() ;
};
ostream& operator<<(ostream& os,Horario h) {
    os << h.hora() << ":" << h.min() ;
    return os;
}
bool Horario::operator<(Horario h) {
    bool mismaHora = hora_ == h.hora();
    bool r   = min_ < h.min();
    return (mismaHora & r) | (hora_ < h.hora()) ;
}
// Ejercicio 13
// Clase Recordatorio
class Recordatorio{
public:
    Recordatorio(Fecha fecha, Horario horario,string msj);
    string msj();
    Fecha fecha();
    Horario horario();

private:
    string msj_;
    Fecha fecha_;
    Horario horario_;
};
Recordatorio::Recordatorio(Fecha fecha, Horario horario,string msj): fecha_(fecha), horario_(horario),msj_(msj) {}
string Recordatorio::msj() {
    return this->msj_;
}
Fecha Recordatorio::fecha() {
    return this->fecha_;
}
Horario Recordatorio::horario(){
    return horario_;
}
ostream& operator<<(ostream& os, Recordatorio rec){
    os << rec.msj() << " @ " <<rec.fecha() << " "<< rec.horario();
    return os;
}

// Ejercicio 14
// TEMA DE LA FECHA ACTUAL  Tengo que llamar a otras librerios para hacerlo?
// Clase Agenda
class Agenda{
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha fecha_actual_;
    Fecha fecha_inicial_;
    list<Recordatorio> recordatorios_;
};
Agenda::Agenda(Fecha fecha_inicial): fecha_actual_(fecha_inicial),fecha_inicial_(fecha_inicial),recordatorios_(list<Recordatorio> {}) {}
void Agenda::incrementar_dia() {
    fecha_actual_.incrementar_dia();
}
Fecha Agenda::hoy() {
    return fecha_actual_;
}
list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> res;
    for (Recordatorio rec : recordatorios_) {
        if (rec.fecha() == fecha_actual_) {
            res.push_back(rec);
        }
    }
    return res;
}

list<Recordatorio> convertir(vector<Recordatorio> v){
    list<Recordatorio> res={};
    for (int i = 0; i < v.size(); ++i) {
        res.push_back(v[i]);
    }
    return res;
}
vector<Recordatorio> inversa(list<Recordatorio> l){
    vector<Recordatorio> res={};
    for(Recordatorio r:l){
        res.push_back(r);
    }
    return res;
}
// Para tener la nocion de orden en los horarios de los recordatorios
list<Recordatorio> ordenar(vector<Recordatorio> v){
    if (v.size() > 1) {
        for (int i = 0; i < v.size(); ++i) {
            for (int j = 1; j < v.size(); ++j) {
                if (v[i].horario() < v[j].horario() || v[i].horario() == v[j].horario()) {} else{
                    Recordatorio reco = v[i];
                    v[i] = v[j];
                    v[j] = reco;
                }
            }
        }
        return convertir(v);
    } else {
        return convertir(v);
    }
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_.push_back(rec);
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl <<"=====" <<endl;
    for (Recordatorio i: ordenar(inversa(a.recordatorios_de_hoy()))) {
        os << i.msj() << " @ " << i.fecha() << " " << i.horario() <<endl;
    }
    return os;
}










