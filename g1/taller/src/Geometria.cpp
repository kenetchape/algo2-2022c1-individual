#include <iostream>

using namespace std;

using uint = unsigned int;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        uint alto_;
        uint ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho) {};

uint Rectangulo::alto() {
    return alto_;
}

uint Rectangulo::ancho() {
    return ancho_;
}
float Rectangulo::area() {
    return ancho_ * alto_;
}
// Ejercicio 2
class Elipse{
    public:
        Elipse(uint r_a, uint r_b);
        uint r_a();
        uint r_b();
        float area();

    private:
        uint radio_a;
        uint radio_b;
};
Elipse::Elipse(uint r_a, uint r_b): radio_a(r_a),radio_b(r_b) {};
uint Elipse::r_a() {
    return radio_a;
}
uint Elipse::r_b() {
    return radio_b;
}
float Elipse::area() {
    return 3.14 * radio_a * radio_b;
}
// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    // Completar
    return r_.alto();
}

float Cuadrado::area() {
    // Completar
    return r_.area();
}

// Ejercicio 4
// Clase Circulo
class Circulo{
    public:
        uint radio();
        float area();
        Circulo(uint radio);

    private:
        Elipse radio_;
};

Circulo::Circulo(uint radio): radio_(radio,radio) {}

uint Circulo::radio() {
    return radio_.r_a();
}
float Circulo::area() {
    return radio_.area();
}


// cout << r << endl;  Rect(10, 15)

// Ejercicio 5
ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", "<< r.ancho()<< ")";
    return os;
}

// ostream Elipse
ostream& operator<<(ostream& os, Elipse e) {
    os << "Elipse(" << e.r_a() << ", "<< e.r_b()<< ")";
    return os;
}

// Ejercicio 6

ostream& operator<<(ostream& os, Circulo c){
    os<< "cric(" << c.radio() << ")";
    return os;
}

ostream& operator<<(ostream& os, Cuadrado c){
    os<< "CUad(" << c.lado() <<")";
    return os;
}

