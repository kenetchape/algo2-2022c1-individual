#include "string_map.h"

template<typename T>
string_map<T>::Nodo::Nodo() {
    for (int i = 0; i < 256; i++) {
        siguientes.push_back(nullptr);
    };
    definicion = nullptr;
}

template<typename T>
string_map<T>::string_map() {
    _size = 0;
    raiz = nullptr;
    SHIFT_ALFAB = 32;
}

// Provisto por la catedra: utiliza el operador asignacion para realizar la copia.
template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar) : string_map() {
    *this = aCopiar;
}

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    if (d.raiz != nullptr) {
        this->raiz = new Nodo();
        this->_size = d._size;
        this->_claves = d._claves;

        // Obtener el vector de la raiz
        vector<Nodo *> v = d.raiz->siguientes;

        // Recorrer el vector de la raiz y por cada nodo, crear uno nuevo y enviar el n->siguientes
        for (int i = 0; i < 256; i++) {
            if (v[i] != nullptr) {
                raiz->siguientes[i] = new Nodo();

                if (v[i]->definicion != nullptr) {
                    T *valor = v[i]->definicion;
                    T *val = new T(*valor);
                    (raiz->siguientes[i])->definicion = val;
                }

                copiarVector(raiz->siguientes[i], v[i]->siguientes);
            }
        }
    }
    return *this;
}

template<typename T>
void string_map<T>::copiarVector(Nodo *n, vector<Nodo *> v) {

    for (int i = 0; i < 256; i++) {
        if (v[i] != nullptr) {
            n->siguientes[i] = new Nodo();

            if (v[i]->definicion != nullptr) {
                T *valor = v[i]->definicion;
                T *val = new T(*valor);
                (n->siguientes[i])->definicion = val;
            }

            copiarVector(n->siguientes[i], v[i]->siguientes);
        }
    }
}

template<typename T>
string_map<T>::~string_map() {
    borrarRama(raiz);
}

template<typename T>
void string_map<T>::borrarRama(Nodo *n) {
    if (n != nullptr) {
        if (n->definicion != nullptr) {
            delete (n->definicion);
        }

        for (int i = 0; i < 256; i++) {
            if (n->siguientes[i] != nullptr) {
                borrarRama(n->siguientes[i]);
            }
        }

        delete (n);
    }
}

//template <typename T>
//T& string_map<T>::operator[](const string& clave){
//    // COMPLETAR
//}


template<typename T>
int string_map<T>::count(const string &clave) const {

    int count_largo = 0;
    // Obtengo el vector del nodo
    if (raiz != nullptr) {
        // vector<Nodo*> v = raiz->siguientes;
        Nodo *n = raiz;

        // recorro la letra
        for (int i = 0; i < clave.size(); i++) {
            // Obtengo el numero de la letra
            int index_clave = clave[i] - SHIFT_ALFAB;

            // Me fijo si en V[clave[I]] != nullptr
            if (n->siguientes[index_clave] != nullptr) {
                // - Si lo es, v = v[clave[i]]->siguientes
                n = n->siguientes[index_clave];
                count_largo++;
            } else {
                break;
            }
        };

        if (count_largo == clave.size() && n->definicion != nullptr) {
            return 1;
        } else {
            return 0;
        }
    }

    return 0;
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &p) {
    // Si la raiz está vacia, creo un nodo y se la asigno
    if (raiz == nullptr) {
        raiz = new Nodo();
    }

    Nodo *n = raiz;

    // Recorro la palabra
    for (int i = 0; i < p.first.size(); i++) {
        int index_clave = p.first[i] - SHIFT_ALFAB;
        // Pregunto si en el vector del nodo está la letra
        if (n->siguientes[index_clave] == nullptr) {
            // Si no está, creo un nodo, y se lo asigno a esa posicion
            Nodo *m = new Nodo();

            n->siguientes[index_clave] = m;
        }
        // Avanzo al siguiente nodo
        n = n->siguientes[index_clave];
    }
    // Asigno la direccion del valor
    if (n->definicion != nullptr) {
        delete n->definicion;
    }
    n->definicion = new T(p.second);

    _size++;
    _claves.insert(p.first);
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo *n = raiz;

    for (int i = 0; i < clave.size() - 1; i++) {
        n = n->siguientes[clave[i] - SHIFT_ALFAB];
    }
    int final_index = clave[clave.size() - 1] - SHIFT_ALFAB;
    T *valor = (n->siguientes[final_index])->definicion;

    return *valor;
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo *n = raiz;

    for (int i = 0; i < clave.size() - 1; i++) {
        n = n->siguientes[clave[i] - SHIFT_ALFAB];
    }
    int final_index = clave[clave.size() - 1] - SHIFT_ALFAB;
    T *valor = (n->siguientes[final_index])->definicion;

    return *valor;
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    Nodo *n = raiz;
    Nodo *no_borrable = raiz;
    int index_no_borrable = 0;

    // Voy al ultimo nodo, y en el camino me guardo el ultimo no borrable
    for (int i = 0; i < clave.size(); i++) {
        int index_clave = clave[i] - SHIFT_ALFAB;

        if (cantHijos(n) > 1 || n->definicion != nullptr) {
            no_borrable = n;
            index_no_borrable = index_clave;
        }

        n = n->siguientes[index_clave];
    }

    // Borro la definicion
    delete (n->definicion);
    n->definicion = nullptr;
    _size--;

    // Hay un nodo anterior y ademas, n no tiene hijos, hay que borrar toda la rama
    if (no_borrable != nullptr && cantHijos(n) == 0) {
        //    int letra_a_borrar = clave[index_no_borrable] - SHIFT_ALFAB;
        Nodo *borrar = no_borrable->siguientes[index_no_borrable];

        borrarRama(borrar);

        no_borrable->siguientes[index_no_borrable] = nullptr;
    }

    _claves.erase(clave);

}

template<typename T>
int string_map<T>::cantHijos(Nodo *n) {
    int cant_nodos = 0;

    for (int i = 0; i < 256; i++) {
        if (n->siguientes[i] != nullptr) {
            cant_nodos++;
        }
    }

    return cant_nodos;
}

template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    return _size == 0;
}

template<typename T>
set<string> string_map<T>::claves() const {
    return _claves;
}